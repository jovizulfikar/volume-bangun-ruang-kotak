<?php

return [

    'SUCCESS'           => ['httpCode'=> 200, 'code' => 'success',            'message' => 'success'],
    'VALIDATION_FAILS'  => ['httpCode'=> 422, 'code' => 'validation-fails',   'message' => 'validation fails, check your inputs.'],
    'UNKNOWN_ERROR'     => ['httpCode'=> 500, 'code' => 'unknown-error',      'message' => 'something went wrong, try again later.']

];