<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Core\Models\Bola;

class BolaTest extends TestCase
{
    use RefreshDatabase;

    private $bolaList;

    public function setUp(): void
    {
        parent::setUp();
        $this->bolaList = factory(Bola::class, 10)->create();
    }

    public function testCreate() 
    {
        $bola = [
            "kode_bangun"   => Str::upper(Str::random(5)),
            "radius"        => rand(1, 100)
        ];

        $response = $this->post('/api/v1/bola', $bola);
        $response->assertJson([
            "result" => [
                "volume" => (4 / 3) * 3.14 * $bola['radius']
            ]
        ]);

        $this->assertDatabaseHas('bola', [
            "kode_bangun"   => $bola['kode_bangun']
        ]);
    }

    public function testGetAll() 
    {
        $response = $this->get('/api/v1/bola');
        $response->assertStatus(200);
    }

    public function testUpdate() 
    {
        $bolaUpdt = [
            "kode_bangun"   => $this->bolaList[0]->kode_bangun,
            "radius"        => rand(1, 100)
        ];

        $resUpdate = $this->patch('/api/v1/bola', $bolaUpdt);
        $resUpdate->assertJson([
            "result" => [
                "volume" => (4 / 3) * 3.14 * $bolaUpdt['radius']
            ]
        ]);
    }

    public function testDelete() 
    {
        $bolaDlt = [
            "kode_bangun"   => $this->bolaList[0]->kode_bangun
        ];

        $resDlt = $this->delete('/api/v1/bola', $bolaDlt);
        $resDlt->assertStatus(200);
        $this->assertSoftDeleted('bola', [
            "kode_bangun"   => $this->bolaList[0]->kode_bangun
        ]);
    }
}
