<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use App\Core\Models\Kotak;
use Tests\TestCase;

class KotakTest extends TestCase
{
    use RefreshDatabase;

    private $kotakList;

    public function setUp(): void
    {
        parent::setUp();
        $this->kotakList = factory(Kotak::class, 10)->create();
    }

    public function testCreate() 
    {
        $kotak = [
            "kode_bangun"   => Str::upper(Str::random(5)),
            "panjang"       => rand(1, 100),
            "lebar"         => rand(1, 100),
            "tinggi"        => rand(1, 100),
        ];

        $response = $this->post('/api/v1/kotak', $kotak);
        $response->assertJson([
            "result" => [
                "volume" => $kotak['panjang'] * $kotak['lebar'] * $kotak['tinggi']
            ]
        ]);

        $this->assertDatabaseHas('kotak', [
            "kode_bangun"   => $kotak['kode_bangun']
        ]);
    }

    public function testGetAll() 
    {
        $response = $this->get('/api/v1/kotak');
        $response->assertStatus(200);
    }

    public function testUpdate() 
    {
        $kotakUpdt = [
            "kode_bangun"   => $this->kotakList[0]->kode_bangun,
            "panjang"       => rand(1, 100),
            "lebar"         => rand(1, 100),
            "tinggi"        => rand(1, 100),
        ];

        $resUpdate = $this->patch('/api/v1/kotak', $kotakUpdt);
        $resUpdate->assertJson([
            "result" => [
                "volume" => $kotakUpdt['panjang'] * $kotakUpdt['lebar'] * $kotakUpdt['tinggi']
            ]
        ]);
    }

    public function testDelete() 
    {
        $kotakDlt = [
            "kode_bangun"   => $this->kotakList[0]->kode_bangun
        ];

        $resDlt = $this->delete('/api/v1/kotak', $kotakDlt);
        $resDlt->assertStatus(200);
        $this->assertSoftDeleted('kotak', [
            "kode_bangun"   => $this->kotakList[0]->kode_bangun
        ]);
    }
}
