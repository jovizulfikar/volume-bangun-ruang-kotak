<?php

use Illuminate\Database\Seeder;
use App\Core\Models\Kotak;

class KotakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Kotak::class, 10)->create();
    }
}
