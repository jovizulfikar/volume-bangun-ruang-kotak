<?php

use Illuminate\Database\Seeder;
use App\Core\Models\Bola;

class BolaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bola::class, 10)->create();
    }
}
