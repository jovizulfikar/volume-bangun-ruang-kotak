<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Core\Models\Kotak;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Kotak::class, function (Faker $faker) {
    return [
        "kode_bangun"   => Str::upper(Str::random(5)),
        "panjang"       => rand(1, 100),
        "lebar"         => rand(1, 100),
        "tinggi"        => rand(1, 100),
    ];
});
