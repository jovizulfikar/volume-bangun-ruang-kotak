<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Core\Models\Bola;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Bola::class, function (Faker $faker) {
    return [
        "kode_bangun"   => Str::upper(Str::random(5)),
        "radius"       => rand(1, 100)
    ];
});
