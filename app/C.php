<?php

namespace App;

class C {
    const SUCCESS                   = "success";
    const VALIDATION_FAILS_STATUS   = "validation-fails";
    const VALIDATION_FAILS_MSG      = "validation fails, check your inputs.";
    const UNKNOWN_ERROR_STATUS      = "unknown-error";
    const UNKNOWN_ERROR_MSG         = "something went wrong, please try again later.";
}