<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateBolaRequest extends FormRequest
{
    protected function failedValidation(Validator $validator) 
    {
        throw new HttpResponseException(response()->json([
            'status'    => config('response.VALIDATION_FAILS')['code'],
            'message'   => config('response.VALIDATION_FAILS')['message'],
            'result'    => $validator->errors()
        ], 422));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_bangun'   => 'required|alpha_num|unique:bola,kode_bangun|max:255',
            'radius'        => 'required|numeric',
        ];
    }
}
