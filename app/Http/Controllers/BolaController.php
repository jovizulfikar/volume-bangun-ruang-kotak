<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Core\Services\BolaService;
use App\Http\Requests\CreateBolaRequest;
use App\Http\Requests\UpdateBolaRequest;
use App\Http\Requests\DeleteBolaRequest;

class BolaController extends Controller
{
    private $bolaService;

    public function __construct(BolaService $bolaService)
    {
        $this->bolaService = $bolaService;
    }

    public function get(Request $request)
    {
        if ($request->query('kode')) {
            return $this->bolaService->getByKode($request->query('kode'));
        } else {
            return $this->bolaService->getAll();
        }
    }

    public function create(CreateBolaRequest $request) 
    {
        $data = $request->only([
            'kode_bangun',
            'radius'
        ]);

        $response = $this->bolaService->create($data);
        $response->setStatusCode(201);

        return $response;
    }

    public function update(UpdateBolaRequest $request) 
    {
        $kodeBangun = $request->kode_bangun;
        $data = $request->only([
            'kode_bangun',
            'radius'
        ]);

        $response = $this->bolaService->update($kodeBangun, $data);

        return $response;
    }

    public function delete(DeleteBolaRequest $request) {
        $kodeBangun = $request->kode_bangun;
        $response = $this->bolaService->delete($kodeBangun);

        return $response;
    }
}
