<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Core\Services\KotakService;
use App\Http\Requests\CreateKotakRequest;
use App\Http\Requests\UpdateKotakRequest;
use App\Http\Requests\DeleteKotakRequest;

class KotakController extends Controller
{
    private $kotakService;

    public function __construct(KotakService $kotakService)
    {
        $this->kotakService = $kotakService;
    }

    public function get(Request $request)
    {
        if ($request->query('kode')) {
            return $this->kotakService->getByKode($request->query('kode'));
        } else {
            return $this->kotakService->getAll();
        }
    }

    public function create(CreateKotakRequest $request) 
    {
        $data = $request->only([
            'kode_bangun',
            'panjang',
            'lebar',
            'tinggi'
        ]);

        $response = $this->kotakService->create($data);
        $response->setStatusCode(201);

        return $response;
    }

    public function update(UpdateKotakRequest $request) 
    {
        $kodeBangun = $request->kode_bangun;
        $data = $request->only([
            'panjang',
            'lebar',
            'tinggi'
        ]);

        $response = $this->kotakService->update($kodeBangun, $data);

        return $response;
    }

    public function delete(DeleteKotakRequest $request) {
        $kodeBangun = $request->kode_bangun;
        $response = $this->kotakService->delete($kodeBangun);

        return $response;
    }
}
