<?php

namespace App\Http\Responses;

class Response {

    public static function create($status, $result = null) {
        
        $code       = config('response.'.$status) ? config('response.'.$status)['code'] : 0;
        $message    = config('response.'.$status) ? config('response.'.$status)['message'] : '';
        $httpCode   = config('response.'.$status) ? config('response.'.$status)['httpCode'] : 200;

        return response()->json([
            'status'    => $code,
            'message'   => $message,
            'result'    => $result
        ], $httpCode);
    }

    public static function exception(\Throwable $e) {
        return response()->json([
            'status'    => config('response.UNKNOWN_ERROR')['code'],
            'message'   => config('response.UNKNOWN_ERROR')['message'],
            'result'    => [
                'code'      => $e->getCode(),
                'message'   => $e->getMessage(),
            ]
        ], 500);
    }

}