<?php

namespace App\Core\Repositories;

use App\Core\Models\Bola;
use Illuminate\Support\Facades\Log;

class BolaRepository 
{

    private $bola;

    public function __construct(Bola $bola)
    {
        $this->bola = $bola;
    }

    public function getAll()
    {
        Log::debug('get all kotak');
        $bolaList = $this->bola->all();
        Log::debug('result', ['data' => $bolaList]);
        return $bolaList;
    }

    public function getByKodeBangun($kodeBangun)
    {
        Log::debug('get bola by kode');
        $bola = $this->bola->where('kode_bangun', $kodeBangun)->first();
        Log::debug('result', ['data' => $bola]);
        return $bola;
    }

    public function insert($bola)
    {
        $newBola = new Bola();
        $newBola->kode_bangun   = $bola['kode_bangun'];
        $newBola->radius        = $bola['radius'];
        $newBola->save();

        Log::debug('insert bola', $bola);
        $newBola->save();
        Log::debug('result', ['data' => $newBola]);
        return $newBola;
    }

    public function update($kodeBangun, $bola)
    {
        Log::debug('update bola', ['kode' => $kodeBangun, 'data' => $bola]);
        $bola = $this->bola->where('kode_bangun', $kodeBangun)->update($bola);
        Log::debug('result', ['data' => $bola]);
        return $bola;
    }

    public function delete($kodeBangun)
    {
        Log::debug('delete bola', ['kode' => $kodeBangun]);
        $result = $this->bola->where('kode_bangun', $kodeBangun)->delete();
        Log::debug('result', ['data' => $result]);
        return $result;
    }

}
