<?php

namespace App\Core\Repositories;

use App\Core\Models\Kotak;
use Illuminate\Support\Facades\Log;

class KotakRepository 
{

    private $kotak;

    public function __construct(Kotak $kotak)
    {
        $this->kotak = $kotak;
    }

    public function getAll()
    {
        Log::debug('get all kotak');
        $kotakList = $this->kotak->all();
        Log::debug('result', ['data' => $kotakList]);
        return $kotakList;
    }

    public function getByKodeBangun($kodeBangun)
    {
        Log::debug('get kotak by kode');
        $kotak = $this->kotak->where('kode_bangun', $kodeBangun)->first();
        Log::debug('result', ['data' => $kotak]);
        return $kotak;
    }

    public function insert($kotak)
    {
        $newKotak = new Kotak();
        $newKotak->panjang      = $kotak['panjang'];
        $newKotak->lebar        = $kotak['lebar'];
        $newKotak->tinggi       = $kotak['tinggi'];
        $newKotak->kode_bangun  = $kotak['kode_bangun'];

        Log::debug('insert kotak', $kotak);
        $newKotak->save();
        Log::debug('result', ['data' => $newKotak]);
        return $newKotak;
    }

    public function update($kodeBangun, $kotak)
    {
        Log::debug('update kotak', ['kode' => $kodeBangun, 'data' => $kotak]);
        $kotak = $this->kotak->where('kode_bangun', $kodeBangun)->update($kotak);
        Log::debug('result', ['data' => $kotak]);
        return $kotak;
    }

    public function delete($kodeBangun)
    {
        Log::debug('delete kotak', ['kode' => $kodeBangun]);
        $result = $this->kotak->where('kode_bangun', $kodeBangun)->delete();
        Log::debug('result', ['data' => $result]);
        return $result;
    }

}
