<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kotak extends Model
{
    use SoftDeletes;

    protected $table = 'kotak';

    protected $fillable = [
        'kode_bangun',
        'panjang',
        'lebar',
        'tinggi',
    ];

    protected $appends = [
        'volume',
    ];

    public function getVolumeAttribute() {
        return $this->panjang * $this->lebar * $this->tinggi;
    }
}
