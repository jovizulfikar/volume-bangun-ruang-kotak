<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bola extends Model
{
    use SoftDeletes;

    protected $table = 'bola';

    protected $fillable = [
        'kode_bangun',
        'radius'
    ];

    protected $appends = [
        'volume',
    ];

    public function getVolumeAttribute() {
        return (4 / 3) * 3.14 * $this->radius;
    }
}
