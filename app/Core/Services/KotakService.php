<?php

namespace App\Core\Services;

use App\Core\Repositories\KotakRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;
use App\C;
use App\Http\Responses\Response;
use Illuminate\Support\Facades\Log;

class KotakService
{
    private $kotakRepository;

    public function __construct(KotakRepository $kotakRepository)
    {
        $this->kotakRepository = $kotakRepository;
    }

    public function getAll()
    {
        try {
            return Response::create('SUCCESS', $this->kotakRepository->getAll());
        } catch(\Throwable $e) {
            Log::error('get all kotak', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function getByKode($kodeBangun) {
        try {
            return Response::create('SUCCESS', $this->kotakRepository->getByKodeBangun($kodeBangun));
        } catch(\Throwable $e) {
            Log::error('get kotak by kode', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function create($kotak)
    {
        DB::beginTransaction();
        try {
            $kotak = $this->kotakRepository->insert($kotak);
            DB::commit();
            return Response::create('SUCCESS', $kotak);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('create kotak', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function update($kodeBangun, $kotak)
    {
        DB::beginTransaction();
        try {
            $this->kotakRepository->update($kodeBangun, $kotak);
            DB::commit();
            $kotak = $this->kotakRepository->getByKodeBangun($kodeBangun);
            return Response::create('SUCCESS', $kotak);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('update kotak', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function delete($kodeBangun)
    {
        DB::beginTransaction();
        try {
            $kotak = $this->kotakRepository->getByKodeBangun($kodeBangun);
            $this->kotakRepository->delete($kodeBangun);
            DB::commit();
            return Response::create('SUCCESS', $kotak);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('delete kotak', ['error' => $e]);
            return Response::exception($e);
        }
    }

}
