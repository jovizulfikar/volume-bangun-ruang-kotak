<?php

namespace App\Core\Services;

use App\Core\Repositories\BolaRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;
use App\Http\Responses\Response;
use Illuminate\Support\Facades\Log;

class BolaService
{
    private $bolaRepository;

    public function __construct(BolaRepository $bolaRepository)
    {
        $this->bolaRepository = $bolaRepository;
    }

    public function getAll()
    {   
        try {
            return Response::create('SUCCESS', $this->bolaRepository->getAll());
        } catch(\Throwable $e) {
            Log::error('get all bola', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function getByKode($kodeBangun) {
        try {
            return Response::create('SUCCESS', $this->bolaRepository->getByKodeBangun($kodeBangun));
        } catch(\Throwable $e) {
            Log::error('get bola by kode', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function create($bola)
    {
        DB::beginTransaction();
        try {
            $bola = $this->bolaRepository->insert($bola);
            DB::commit();
            return Response::create('SUCCESS', $bola);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('create bola', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function update($kodeBangun, $bola)
    {
        DB::beginTransaction();
        try {
            $this->bolaRepository->update($kodeBangun, $bola);
            DB::commit();
            $bola = $this->bolaRepository->getByKodeBangun($kodeBangun);
            return Response::create('SUCCESS', $bola);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('update bola', ['error' => $e]);
            return Response::exception($e);
        }
    }

    public function delete($kodeBangun)
    {
        DB::beginTransaction();
        try {
            $bola = $this->bolaRepository->getByKodeBangun($kodeBangun);
            $this->bolaRepository->delete($kodeBangun);
            DB::commit();
            return Response::create('SUCCESS', $bola);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::error('delete bola', ['error' => $e]);
            return Response::exception($e);
        }
    }

}
