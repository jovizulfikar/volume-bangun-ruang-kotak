<?php

use Illuminate\Support\Facades\Route;

Route::prefix('kotak')->group(function() {
	Route::get('', 'KotakController@get');
	Route::post('', 'KotakController@create');
	Route::patch('', 'KotakController@update');
	Route::delete('', 'KotakController@delete');
});

Route::prefix('bola')->group(function() {
	Route::get('', 'BolaController@get');
	Route::post('', 'BolaController@create');
	Route::patch('', 'BolaController@update');
	Route::delete('', 'BolaController@delete');
});